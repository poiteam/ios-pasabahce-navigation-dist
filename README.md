![Alamofire: Elegant Networking in Swift](https://www.poilabs.com/public/img/poi-labs-logo.png)

# PoilabsNavigation Sdk

## Instalation

### PoilabsNavigation

#### Manually

Proje Dosyalarında düzenlenmesi gereken değerler aşağıda gösterilmiştir:

* **.framework** dosyalarını General Sayfasında Embedded Binaries Başlığı altına ekleyiniz.

	*  PoilabsNavigation.framework

* **.bundle** dosyasını da Build Phases  Sayfasında Copy bundle Resources Başlığı altına ekleyiniz.
	*  PoilabsNavigationResources.bundle
    
#### CocoaPods

``` curl
pod 'PoilabsPasabahceNavigation', '~> 1.0.26'
```

### Mapbox
``` curl
pod 'Mapbox-iOS-SDK', '~> 5.9.0'
```
## Usage

### Objective C

Harita ViewController yapısındadır. Haritayı göstermek için loadMap methodunu kullanarak ekranı uygulamanıza ekleyebilirsiniz. 

getReadyForStoreMapWithCompletionHandler methodunda asenkron işlem yapılmaktadır. Completion handler'ı gelene kadar HUD göstermenizde fayda olacaktır.

Haritayı belli pointleri göstererek başlatmak için [PLNNavigationSettings sharedInstance].storeIds listini istediğiniz pointlerin id leri ile set etmeniz gerekmektedir. Eğer bu listi nil bırakırsanız harita tüm pointleri gösterecek şekilde açılacaktır.

Kullanıcının anlık konumunu dinlemek için userLocationDelegate'i set etmeniz gerekmektedir.

```objective-c
#import <PLNavigation/PLNavigation.h>
#import <PoilabsNavigation/PLNLocationManager.h>

@interface ViewController ()<PLNNavigationMapViewDelegate, PLNUserLocationDelegate>

@property(strong, nonatomic) PLNNavigationMapView *currentCarrier;
@property (weak, nonatomic) IBOutlet UIView *mapView;

@end

-(void)loadMap: {
	[[PLNNavigationSettings sharedInstance] setApplicationId:@"application id"];
	[[PLNNavigationSettings sharedInstance] setApplicationSecret:@"application secret id"];
	[[PLNNavigationSettings sharedInstance] setNavigationUniqueIdentifier:@"unique identifier"];
	
	//aplication language tr/en
	[[PLNNavigationSettings sharedInstance] setApplicationLanguage:@"tr"];
	
	//Haritada gösterilecek pointlerin set edilmesi
	NSMutableArray<NSString *> *ids = [[NSMutableArray alloc] init];
	[ids addObject:@"pointId1"];
	[ids addObject:@"pointId2"];
	[PLNNavigationSettings sharedInstance].storeIds = ids;

	//show loading animation
	[[PLNavigationManager sharedInstance] getReadyForStoreMapWithCompletionHandler:^(PLNError *error) {
	
		//hide loading animation

		PLNNavigationMapView *carrierView = [[PLNNavigationMapView alloc] initWithFrame:
                                             CGRectMake(0, 10, self.mapView.bounds.size.width, self.mapView.bounds.size.height -20)];
		[carrierView awakeFromNib];
		carrierView.delegate = self;
		self.currentCarrier = carrierView;
		[self.mapView addSubview:carrierView];

		//userLocationDelegate'in set edilmesi
		[[PLNLocationManager sharedInstance] setUserLocationDelegate:self];
    }];
}

```

Kullanıcının anlık konumu userLocation:(NSString *)physicalId methodu ile dinlenir. Kullanıcının o an bulunduğu point'in id'si bu method'a düşer.

```objective-c

-(void)userLocation:(NSString *)physicalId {
	NSLog(physicalId);
}

```

Sdk çalışmaya başladıktan sonra herhangi bir an harita üzerinde istenilen pointlerin gösterilmesi için -(void)setLocationIcons: (NSArray<NSString *> *)storeIds method'u kullanılır. 

```objective-c
	NSMutableArray<NSString *> *ids = [[NSMutableArray alloc] init];
	[ids addObject:@"pointId1"];
	[ids addObject:@"pointId2"];
	[[PLNNavigationSettings sharedInstance] setLocationIcons:ids];
```
Sdk çalışmaya başladıktan sonra kullanıcının bulunduğu konumdan herhangi bir konuma rota almak için -(void)getRoute:(NSString *)target methodu kullanılır. Bu method sadece haritanın yüklendiği ve konumun bulunduğu durumda çalışır.

```objective-c
    [[PLNNavigationSettings sharedInstance] getRoute:@"storeId"];
```
Sdk çalışmaya başladıktan sonra haritayı herhangi bir noktaya yönlendirmek için -(void)getShowonMapPin:(NSString *)poiId methodu kullanılır.

```objective-c
    [[self currentCarrier] getShowonMapPin:@"storeId"];
    self.currentCarrier?.getShowonMapPin("storeId")
```

### Swift

Harita ViewController yapısındadır. Haritayı göstermek için loadMap methodunu kullanarak ekranı uygulamanıza ekleyebilirsiniz.

getReadyForStoreMap methodunda asenkron işlem yapılmaktadır. Completion handler'ı gelene kadar HUD göstermenizde fayda olacaktır.

Haritayı belli pointleri göstererek başlatmak için PLNNavigationSettings.sharedInstance()?.storeIds listini istediğiniz pointlerin id leri ile set etmeniz gerekmektedir. Eğer bu listi nil bırakırsanız harita tüm pointleri gösterecek şekilde açılacaktır.

Kullanıcının anlık konumunu dinlemek için userLocationDelegate'i set etmeniz gerekmektedir.

Haritayı fırsat pointleri göstererek başlatmak için PLNNavigationSettings.sharedInstance()?.starPoints listini istediğiniz pointlerin id leri ile set etmeniz gerekmektedir. Eğer bu listi nil bırakırsanız harita tüm pointleri gösterecek şekilde açılacaktır.


```swift
import PoilabsNavigation
class ViewController: UIViewContoller {

@IBOutlet weak var navigationView: UIView!
var currentCarrier: PLNNavigationMapView?

    func loadMap() {
        PLNNavigationSettings.sharedInstance().applicationId = "application id"
        PLNNavigationSettings.sharedInstance().applicationSecret = "application secret"
        PLNNavigationSettings.sharedInstance()?.navigationUniqueIdentifier = "unique identifier"
	
	//aplication language tr/en
	PLNNavigationSettings.sharedInstance()?.applicationLanguage = "tr"
        
	//Haritada gösterilecek pointlerin set edilmesi
	var storeIds = [String]()
        storeIds.append("storeId1")
	storeIds.append("storeId2")
        PLNNavigationSettings.sharedInstance()?.storeIds = storeIds
        
        
        var starIds = [String]()
        starIds.append("storeId1")
        starIds.append("storeId2")
        starIds.append("storeId3")
        PLNNavigationSettings.sharedInstance()?.starPoints = starIds
        
	//show loading animation
        PLNavigationManager.sharedInstance()?.getReadyForStoreMap(completionHandler: { (error) in
		
		//hide loading animation

		let carrierView = PLNNavigationMapView(frame: CGRect(x: 0, y: 0, width: self.navigationView.bounds.size.width, height: self.navigationView.bounds.size.height))
		carrierView.awakeFromNib()
		carrierView.delegate = self as? PLNNavigationMapViewDelegate
		self.currentCarrier = carrierView
		self.navigationView.addSubview(carrierView)

		//userLocationDelegate'in set edilmesi
		PLNLocationManager.sharedInstance()?.userLocationDelegate = self
        })
    }
}
```

Kullanıcının anlık konumu userLocation(_ physicalId: String!) methodu ile dinlenir. Kullanıcının o an bulunduğu point'in id'si bu method'a düşer.

```swift
extension ViewController: PLNUserLocationDelegate {
    func userLocation(_ physicalId: String!) {
        print(physicalId)
    }
}

```

Sdk çalışmaya başladıktan sonra herhangi bir an harita üzerinde istenilen pointlerin gösterilmesi için -(void)setLocationIcons: (NSArray<NSString *> *)storeIds method'u kullanılır.

```swift
        var storeIds = [String]()
        storeIds.append("storeId1")
        storeIds.append("storeId2")
        PLNNavigationSettings.sharedInstance()?.setLocationIcons(storeIds)

```

Sdk çalışmaya başladıktan sonra kullanıcının bulunduğu konumdan herhangi bir konuma rota almak için -(void)getRoute:(NSString *)target methodu kullanılır. Bu method sadece haritanın yüklendiği ve konumun bulunduğu durumda çalışır.

```swift
    PLNNavigationSettings.sharedInstance()?.getRoute("storeId")
```

Sdk çalışmaya başladıktan sonra haritayı herhangi bir noktaya yönlendirmek için -(void)getShowonMapPin:(NSString *)poiId methodu kullanılır.

```swift
    self.currentCarrier?.getShowonMapPin("storeId")
```

### SDK’nın çalışması için Info.plist dosyasına eklenmesi gerekenler:

+MGLMapboxMetricsEnabledSettingShownInApp : YES

+Privacy - Location Usage Description: ‘Sizin açıklamanız’

+Privacy - Location When In Use Usage Description: ‘Sizin açıklamanız’

+Privacy - Location Always Usage Description: ‘Sizin açıklamanız’

+Privacy - Location Always and When In Use Usage Description: ‘Sizin açıklamanız’

+Privacy - Bluetooth Peripheral Usage Description: ‘Sizin açıklamanız’

+Privacy - Bluetooth Always Usage Description: ‘Sizin açıklamanız’
