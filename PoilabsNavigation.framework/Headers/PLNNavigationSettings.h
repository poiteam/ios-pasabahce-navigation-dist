//
//  PLNNavigationSettings.h
//  PoiNavigationMainApp
//
//  Created by ERCAN AYYILDIZ on 05/04/2017.
//  Copyright © 2017 poilabs. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PLNUserLocationDelegate.h"

@interface PLNNavigationSettings : NSObject

+ (instancetype)sharedInstance;

@property(strong, nonatomic) NSString *applicationId;

@property(strong, nonatomic) NSString *applicationSecret;

@property(strong, nonatomic) NSString *navigationUniqueIdentifier;

@property(strong, nonatomic) NSString *mallId;

// set tr or en, default is tr
@property(strong, nonatomic) NSString *applicationLanguage;

@property(strong, nonatomic) NSArray<NSString *> *storeIds;

@property(strong, nonatomic) NSArray<NSString *> *starPoints;

@property(strong, nonatomic) NSString *targetStoreId;

-(void)setLocationIcons: (NSArray<NSString *> *)storeIds;

-(void)getRoute:(NSString *)target;


//optional
@property(assign, nonatomic) float backButtonWidth;

@property(assign, nonatomic) float searchCancelWidth;



@end
