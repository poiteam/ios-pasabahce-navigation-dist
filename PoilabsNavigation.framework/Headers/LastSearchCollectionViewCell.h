//
//  LastSearchCollectionViewCell.h
//  PoilabsNavigation
//
//  Created by Emre Kuru on 17.08.2020.
//  Copyright © 2020 poilabs. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LastSearchCollectionViewCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UIButton *deleteButton;
@property (weak, nonatomic) IBOutlet UIButton *selectButton;


@end
